import React from 'react'
import './Lista.css'
import {useImmer} from 'use-immer'

export default function Todo() {
    
        const [list, setList] = useImmer([]);
        const [filtro, setFiltro] = React.useState('all');
        const input = React.useRef();

    React.useEffect(() => {
        let load = JSON.parse(localStorage.getItem("lista")) || [];
        setList(() => load)
    }, [])

    React.useEffect( ()=>{
        if (list === null) return;

        localStorage.setItem("lista", JSON.stringify(list))
    }
    ,[list])
    
    const insert = (e) => {
        e.preventDefault();

        setList(list => {list.push({ favorito:false, done:false, label: input.current.value })})
        
    }

    const toggle = (index) => {
        setList(list => {list[index].done = !list[index].done});
    }

    const removeTodo = (index) => {
        setList((list) => {
            list.splice(index, 1)
        })
    }
    
    const favoritar = (index) => {
        setList(item => {item[index].favorito = !item[index].favorito})
    }
    
    if (list == null) return null;

    return(
        <div>
            <h1>Todo-List</h1>

            <div>
                <button onClick={ev => setFiltro('all')}>Tudo</button>
                <button onClick={ev => setFiltro('fav')}>Favoritos</button>
                <button onClick={ev => setFiltro('conc')}>Concluidos</button>
            </div>

            <ul>
                {list.filter((item) => {
                    if (filtro === 'all') return true;
                    if (filtro === 'fav') return item.favorito;
                    if (filtro === 'conc') return item.done;
                    return false;
                })
                .map((task, i) => (
                    <>
                    <li onClick={ev => toggle(i)} className={task.done ? "task-done" : ""} key={i}>{task.label}</li>
                    <button onClick={ev => removeTodo(i)}>Apagar</button>
                    <button onClick={ev => favoritar(i)}>{task.favorito ? 'Desfavoritar' : 'Favoritar'}</button>
                    </>
                ))}
            </ul>

            <form onSubmit={insert}>
                <input ref={input} type="text" name="label"/>
                <button type="submit">Adicionar</button>
            </form>
        </div>
    )
}